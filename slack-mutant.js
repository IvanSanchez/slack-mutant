
// 1: Read the code
// 2: Understand that it's dangerous to copy-paste code into a dev console in
//    your browser if you don't know what it does.
// 3: Go to a Slack channel
// 4: Copy-paste this code into a dev console.
// 5: Wait for somebody to write something in the channel
// 6: See the message bits in your dev console


// Callback to be executed when mutations happen
function onMutations(mutations) {
	// Just loop through mutations, looking for newly added <ts-message> nodes
	for (var i = 0; i < mutations.length; ++i) {
		var mutation = mutations[i];
		for (var j = 0; j < mutation.addedNodes.length; ++j) {	// Only looks for added nodes, not modified!!
			var node = mutation.addedNodes[j];

			if (node.tagName === 'TS-MESSAGE') {
				onMutatedMsg(node);
			} else if (node instanceof HTMLElement){
				Array.prototype.forEach.call(node.querySelectorAll('ts-message'), onMutatedMsg);
			}
		};
	};
}

// Callback to be executed on each mutated node
function onMutatedMsg(node){
	console.log(node);
// 	console.log(node.innerHTML);	// Too noisy, but good for dev

	// I'm sure there are more data bits that can be extracted
	var body = node.querySelector('span.message_body').innerHTML;
	var sender = node.querySelector('a.message_sender').innerHTML;
	var timestamp = node.dataset.ts;

	console.log(timestamp, sender, body);
}


// Hook up all the thing
var messageContainer = document.getElementById('messages_container');

if (!messageContainer) {
	throw new Error('Message container not found :-(');
}

var observer = new MutationObserver(onMutations);

observer.observe(messageContainer, { childList: true, subtree: true });
